a tale of redux middleware called
#### redux-saga

---

created by
### <span style="color: #3023AE">Yassine Elouafi</span>

---

### so what are <span style="color: #3023AE">Sagas</span>?

---

### <span style="color: #3023AE">Side Effects</span>

---

### Better than <span style="color: #3023AE">redux-thunk</span>?

---

redux-saga uses
### <span style="color: #3023AE">ES6 Generators</span>
`function*` `yield`

> Great for testing

---

### But how does it work?
with your basic redux dispatch
listener sagas
worker sagas
with your basic redux reducer
